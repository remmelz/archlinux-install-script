#!/bin/bash

set -x 

_disk=${1}
_uuid=${2}

# Install default packages
pacman --noconfirm -Sy \
  networkmanager lvm2 sudo which python \
  man bash-completion openssh elinks neofetch

# Enable services
systemctl enable NetworkManager
systemctl enable sshd

# SSH config
#sed -i '/PermitRootLogin/aPermitRootLogin yes'        /etc/ssh/sshd_config
#sed -i '/PermitRootLogin/aPasswordAuthentication yes' /etc/ssh/sshd_config

# Set timezone
ln -sf /usr/share/zoneinfo/Europe/Amsterdam /etc/localtime
hwclock --systohc

# Set locale
sed -i 's/^#en_US.UTF/en_US.UTF/g' /etc/locale.gen
locale-gen

# Set hostname
echo 'archlinux.home.lan' > /etc/hostname

# Disable speaker
echo 'blacklist pcspkr' > /etc/modprobe.d/nobeep.conf

# Create new initramfs
sed -i 's/block filesystems/block encrypt lvm2 filesystems/g' /etc/mkinitcpio.conf
mkinitcpio -P

# Install GRUB
pacman --noconfirm -Sy grub efibootmgr

sed -i 's/GRUB_TIMEOUT=.*/GRUB_TIMEOUT=1/g' /etc/default/grub
sed -i "s|GRUB_CMDLINE_LINUX=.*|GRUB_CMDLINE_LINUX=\"cryptdevice=${_uuid}:cryptlvm root=/dev/sysvg/rootlv\"|g" /etc/default/grub

if [[ -n $(echo ${_disk} | grep nvme) ]]
then
  # UEFI boot
  grub-install \
    --target=x86_64-efi \
    --efi-directory=/boot \
    --bootloader-id=GRUB \
    --recheck
else
  # Legacy boot
  grub-install --target=i386-pc /dev/${_disk}
fi

grub-mkconfig -o /boot/grub/grub.cfg

# Set root password
echo 'root:toor' | chpasswd


