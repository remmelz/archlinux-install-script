#!/bin/bash

set -x

# Set ntp
timedatectl set-ntp true

# Retrieve the disk name
_disk=`lsblk --raw \
  | grep -E 'G .*disk' \
  | awk -F' ' '{print $1}'`

# Get total disk size
_size_total=`lsblk --raw /dev/${_disk} \
  | grep -E 'G .*disk' \
  | awk -F' ' '{print $4}' \
  | sed 's/G//g'`
_size_total=`echo "${_size_total}-1" | bc`

# Retrieve memory size
_size_ram=`free -g \
  | grep -i '^mem:' \
  | awk -F' ' '{print $2}'`

# Delete index
dd if=/dev/zero of=/dev/${_disk} bs=1M count=10

# Format the disk
fdisk /dev/${_disk} <<EOL
n
p


+700M
p
n
p



y
p
w
EOL

# When disk is nvme
if [[ -n $(echo ${_disk} | grep nvme) ]]
then
  _disk=${_disk}p
fi

# Encrypt partition
cryptsetup luksFormat /dev/${_disk}2
cryptsetup open /dev/${_disk}2 cryptlvm

# Calculate partitions
_size_swap=1
_size_root=`echo "${_size_total}*0.4" | bc | cut -d. -f1`
_size_buff=`echo "${_size_total}*0.2" | bc | cut -d. -f1`
[[ ${_size_ram}  -gt 8  ]] && _size_swap=2
[[ ${_size_root} -lt 10 ]] && _size_root=10
[[ ${_size_root} -gt 30 ]] && _size_root=30
[[ ${_size_buff} -gt 30 ]] && _size_buff=30
_size_home=`echo "${_size_total}-${_size_swap}-${_size_root}-${_size_buff}" | bc | cut -d. -f1`

# Create LVM partitions (all=100%FREE)
pvcreate /dev/mapper/cryptlvm
vgcreate sysvg /dev/mapper/cryptlvm
lvcreate -n swaplv -L ${_size_swap}G sysvg
lvcreate -n rootlv -L ${_size_root}G sysvg
lvcreate -n homelv -L ${_size_home}G sysvg

# Format the partitions
mkfs.fat -F32 /dev/${_disk}1
mkfs.ext4 /dev/sysvg/rootlv
mkfs.ext4 /dev/sysvg/homelv
mkswap /dev/sysvg/swaplv

# Mount the partitions
mount /dev/sysvg/rootlv /mnt
mkdir /mnt/home
mkdir /mnt/boot
mount /dev/sysvg/homelv /mnt/home
mount /dev/${_disk}1 /mnt/boot
swapon /dev/sysvg/swaplv

# Show partitions
echo '--- Filesystem ---'
lsblk
vgs
lvs

# Pause
echo
read -p "Enter to start installing base system."

# Refresh keys
pacman --noconfirm -Sy archlinux-keyring

# Install base packages
pacstrap /mnt base linux linux-firmware vim

# Generate fstab
genfstab -U /mnt >> /mnt/etc/fstab

# Get UUID
_uuid=$(blkid | grep ${_disk}2 | awk -F' ' '{print $2}' | sed 's/"//g')

# Pause
echo
read -p "Enter to start installing archlinux."

# Execute script in chroot mode
cp chroot.sh /mnt/
arch-chroot /mnt /chroot.sh ${_disk} ${_uuid}

# Unmount disks
umount -R /mnt
swapoff -a


